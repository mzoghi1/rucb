\begin{proof}[Proof of Lemma \ref{lem:BetaEstimate}]
Let $S_n = \sum_{i=1}^n X_i \sim B(n,p)$ and $T_{n+1} = \sum_{i=1}^{n+1} Y_i \sim B\left(n+1,\frac{1}{2}\right)$ be binomial random variables.
Since $\Pi^n$ depends on $S_n$, to obtain $P(\theta > \frac{1}{2}|n)$, we need to marginalize over $S_n$. In what follows, $F^{D}$ indicates the CDF of the one-dimensional probability distribution $D$.
\begin{align*}
P\left(\theta > \frac{1}{2}\bigg|n\right) & = \sum_{j=0}^n P\left(\theta > \frac{1}{2}\bigg|S_n=j\right) P(S_n=j) \\
        & \nngsp = \sum_{j=0}^n \left(1 - P\left(\Pi^n < \frac{1}{2}\bigg|S_n=j\right) \right) P(S_n=j) \\
        & \nngsp = \sum_{j=0}^n \left(1 - F^{Beta}_{j+1,n-j+1}\left(\frac{1}{2}\right) \right) P(S_n=j) \\
        & \nngsp = \sum_{j=0}^n F^B_{n+1,\frac{1}{2}} (j) P(S_n=j) \\
        & \nngsp \qquad \because\text{Fact 1 in \cite{Agrawal:2012}} \\
        & \nngsp = \sum_{j=0}^n \sum_{i=0}^j P(T_{n+1}=i) P(S_n=j), \\
%        & \nngsp \qquad\qquad \text{where $T \sim B\left(n+1,\frac{1}{2}\right)$} \\
        & \nngsp = \sum_{j=0}^n \sum_{i=n+1-j}^{n+1} P(T_{n+1}=i) P(S_n=j), \\
        & \nngsp \qquad\qquad \because\text{ $P(T_{n+1}=j)=P(T_{n+1}=n+1-j)$} \\
        & \nngsp = \sum_{k=n+1}^{2n+1} \sum_{i=k-n-1}^{n} P(T_{n+1}=i) P(S_n=k-i) \\
        & \nngsp = \sum_{k=n+1}^{2n+1} P(S_n+T_{n+1}=k) \\
        & \nngsp = P(S_n+T_{n+1} \geq n+1)
\end{align*}
\end{proof}

\begin{proof}[Proof of Lemma \ref{lem:BetaTailShrinkage}] Let us begin by marginalizing over the hidden variables: to do so, we introduce the random variables $S$ and $F$, which count the number of $1$'s and $0$'s among the $X_i$'s, respectively, and we will use the notation $p(s,f) := P(S=s,F=f)$. We will also use the notations $q:=1-p$ and $\beta_{s,f} := P\left(B(s+1,f+1) > \frac{1}{2}\right)$. Armed with this, we can press on as follows:
\begin{align*}
P\left(\Pi^n > \frac{1}{2}\right) & = p(n,0)\beta_{n,0}+p(n-1,1)\beta_{n-1,1}+\cdots+p(1,n-1)\beta_{1,n-1}+p(0,n)\beta_{0,n} \\
 & = p(n,0)[p+q]\beta_{n,0}+p(n-1,1)[p+q]\beta_{n-1,1}+ \\
 & \qquad\qquad\qquad\qquad \cdots+p(1,n-1)[p+q]\beta_{1,n-1}+p(0,n)[p+q]\beta_{0,n} \\
P\left(\Pi^{n+1} > \frac{1}{2}\right) & = p(n+1,0)\beta_{n+1,0}+p(n,1)\beta_{n,1}+\cdots+p(1,n)\beta_{1,n}+p(0,n+1)\beta_{0,n+1} \\
 & = p(n,0)p\beta_{n+1,0}+\left[p(n,0)q+p(n-1,1)p\right]\beta_{n,1}+ \\
 & \qquad\qquad\qquad\qquad \cdots + \left[p(1,n-1)q+p(0,n)p\right]\beta_{1,n} + p(0,n)q\beta_{0,n+1},
\end{align*}
where the last equality is got from the product rule and the fact that $P(X_{n+1}=0)=q$ and $P(X_{n+1}=1)=p$. In order to proceed, we will make use the following two definitions: $S_{s,f} := \beta_{s+1,f}-\beta_{s,f}$ and $F_{s,f} := \beta_{s,f+1}-\beta_{s,f}$. Let us also point out a few important facts about these quantities:
\begin{itemize}
\item[i.] $S_{a,b} = -F_{b,a}$: this is because $\beta_{a,b}=1-\beta_{b,a}$, which in turn is due to the fact that the pdf of $B(b,a)$ and that of $B(a,b)$ are mirror images of each other about the vertical line passing through $\frac{1}{2}$.
\item[ii.] $S_{a,b} > 0$ for all $a$ and $b$.
\end{itemize}

Moreover, let us note that $p(s,t) = \binom{s+t}{s} p^s q^t = \binom{s+t}{t} p^s q^t$. Given these facts and the above pair of equalities, we can write

\begin{align*}
P\left(\Pi^{n+1} > \frac{1}{2}\right) - P\left(\Pi^n > \frac{1}{2}\right) & = p(n,0)pS_{n,0}+p(n,0)qF_{n,0}+p(n-1,1)pS_{n-1,1} + p(n-1,1)qF_{n-1,1} + \\
& \nngsp\nnngsp \qquad\quad \cdots + p(1,n-1)pS_{1,n-1} + p(1,n-1)qF_{1,n-1} + p(0,n)pS_{0,n} + p(0,n)qF_{0,n}\\
& \nngsp\nnngsp = p(n,0)pS_{n,0} - p(n,0)qS_{0,n} + p(n-1,1)pS_{n-1,1} -p(n-1,1)qS_{1,n-1} + \\
& \nngsp\nnngsp \qquad\quad \cdots + p(1,n-1)pS_{1,n-1} - p(1,n-1)qS_{n-1,1} + p(0,n)pS_{0,n} - p(0,n)qS_{n,0} \\
& \nngsp\nnngsp = \binom{n}{0}p^npS_{n,0} - \binom{n}{0}p^nqS_{0,n} + \binom{n}{1}p^{n-1}qpS_{n-1,1} - \binom{n}{1}p^{n-1}qqS_{1,n-1} + \\
& \nngsp\nnngsp \qquad\quad \cdots + \binom{n}{n-1}pq^{n-1}pS_{1,n-1} - \binom{n}{n-1}pq^{n-1}qS_{n-1,1} + \binom{n}{n}q^npS_{0,n} - \binom{n}{n}q^nqS_{n,0} \\
& \nngsp\nnngsp = p^{n+1}S_{n,0} - p^nqS_{0,n} + \binom{n}{1}p^{n}qS_{n-1,1} - \binom{n}{1}p^{n-1}q^2S_{1,n-1} + \binom{n}{2}p^{n-1}q^2S_{n-2,2} + \\
& \nngsp\nnngsp \qquad\quad \cdots - \binom{n}{2}p^2q^{n-1}S_{n-2,2} + \binom{n}{1}p^2q^{n-1}S_{1,n-1} - \binom{n}{1}pq^{n}S_{n-1,1} + pq^nS_{0,n} - q^{n+1}S_{n,0} \\
= S_{n,0}\left[p^{n+1} - q^{n+1}\right] & + pq\left(\binom{n}{1}S_{n-1,1}-S_{0,n}\right)\left[p^{n-1}-q^{n-1}\right] \\
& + p^2q^2\left(\binom{n}{2}S_{n-2,2}-\binom{n}{1}S_{1,n-1}\right)\left[p^{n-3}-q^{n-3}\right] \\
+ \cdots & + p^kq^k\left(\binom{n}{k}S_{n-k,k}-\binom{n}{k-1}S_{k-1,n-k+1}\right)\left[p^{n+1-2k}-q^{n+1-2k}\right] \\
+ \cdots & + \left\{
\begin{array}{ll}
(pq)^{\frac{n}{2}}\left(\binom{n}{\frac{n}{2}}S_{\frac{n}{2},\frac{n}{2}}-\binom{n}{\frac{n}{2}-1}S_{\frac{n}{2}-1,\frac{n}{2}+1}\right)\left[p-q\right]  & \text{if $n$ is even} \\
(pq)^{\frac{n+1}{2}}\left(\binom{n}{\frac{n-1}{2}}S_{\frac{n-1}{2},\frac{n+1}{2}}-\binom{n}{\frac{n+1}{2}}S_{\frac{n-1}{2},\frac{n+1}{2}}\right) = 0 & \text{if $n$ is odd}
\end{array} \right.
\end{align*}
To show that this quantity is negative, hence proving the claim, we note that $p < \frac{1}{2}$ and so $q = 1-p > p$, and so it suffices to show that for each $k$, the term $\binom{n}{k}S_{n-k,k}-\binom{n}{k-1}S_{k-1,n-k+1}$ is positive, and we will show this by first noting that $k < n/2$ and so $\binom{n}{k-1} < \binom{n}{k}$, which means that the positivity of $\binom{n}{k}S_{n-k,k}-\binom{n}{k-1}S_{k-1,n-k+1}$ follows from the fact that $S_{n-k,k}=S_{k-1,n-k+1}$, which is what we will show next. But, first, let us introduce the notation $F^{beta}_{a,b}(x)$ for the CDF of the Beta distribution $Beta(a,b)$ and $F^B_{n,p}(k)$ for the CDF of the Binomial distribution $B(n,p)$. Now, from the definition of $\beta_{s,f}$, we have
\begin{align*}
\beta_{s,f} & = 1 - F^{beta}_{s+1,f+1}\left(\frac{1}{2}\right) \\
            & = F^B_{s+f+1,\frac{1}{2}}(s) \qquad \because\text{ Fact 1 in \cite{Agrawal:2012}}
\end{align*}
Therefore, we have
\begin{align*}
S_{s,f} & = \beta_{s+1,f}-\beta_{s,f} \\
        & = F^B_{s+f+2,\frac{1}{2}}(s+1) - F^B_{s+f+1,\frac{1}{2}}(s)
\end{align*}
Now let us look at the difference $S_{n-k,k}-S_{k-1,n-k+1}$, bearing in mind that $k \leq n/2$:
\begin{align*}
S_{n-k,k}-S_{k-1,n-k+1} & = F^B_{n+2,\frac{1}{2}}(n-k+1) - F^B_{n+1,\frac{1}{2}}(n-k) -\left[ F_{n+2,\frac{1}{2}}(k) - F^B_{n+1,\frac{1}{2}}(k-1) \right] \\
& = F^B_{n+2,\frac{1}{2}}(n-k+1) - F_{n+2,\frac{1}{2}}(k) -\left[ F^B_{n+1,\frac{1}{2}}(n-k) - F^B_{n+1,\frac{1}{2}}(k-1) \right] \\
& = \frac{1}{2^{n+2}} \left[ \binom{n+2}{k+1} + \cdots + \binom{n+2}{n-k+1} \right] - \frac{1}{2^{n+1}} \left[ \binom{n+1}{k} + \cdots + \binom{n+1}{n-k} \right] \\
& = \frac{\left[ \binom{n+2}{k+1} + \cdots + \binom{n+2}{n-k+1} \right] - 2 \left[ \binom{n+1}{k} + \cdots + \binom{n+1}{n-k} \right]}{2^{n+2}} \\
& = \frac{\left[ \underline{\binom{n+2}{k+1}} + \cdots + \underline{\underline{\binom{n+2}{n-k+1}}} \right] - \left[ \underline{\binom{n+1}{k}} + \cdots + \underline{\underline{\binom{n+1}{n-k}}} \right] - \left[ \underline{\underline{\binom{n+1}{n-k+1}}} + \cdots + \underline{\binom{n+1}{k+1}} \right]}{2^{n+2}} \qquad (\dagger) \\
& = \frac{\cancel{\left( \underline{\binom{n+2}{k+1} - \binom{n+1}{k} - \binom{n+1}{k+1}} \right)} + \cdots + \cancel{\left( \underline{\underline{\binom{n+2}{n-k+1} - \binom{n+1}{n-k} - \binom{n+1}{n-k+1}}} \right)}}{2^{n+2}} \qquad (\ddagger)
\end{align*}
Here are the explanations for the steps marked by $\dagger$ and $\ddagger$:
\begin{itemize}
\item[$\dagger$:] In this step, we made use of the fact that $binom{n}{k} = \binom{n}{n-k}$ for all $n$ and $k$.
\item[$\ddagger$:] First of all the underlines are used keep track of the terms that have been grouped together. Moreover, the cancellations in this step are obtained by using using the following well-known relation between binomial numbers:
\[ \binom{n+1}{k+1} = \binom{n}{k} + \binom{n}{k+1}. \]    
\end{itemize}
\end{proof}

\begin{proof}[Proof of Lemma \ref{lem:BinomialEstimate}]
First consider the right-most inequality in Expression \eqref{eqn:LUB}, whose proof is a simple consequence of the Hoeffding bound \citep[Theorem 2]{Hoeffding:1963}. Since $X_i+Y_i$ has mean $\mu := p+\frac{1}{2}=1-\Delta$ (since $\Delta := \frac{1}{2}-p$) and only takes values between $0$ and $2$, the Hoeffding bound can be written as:
%
\[ P\left(\sum_{i=1}^n X_i+Y_i \geq n\mu + \epsilon \right) \leq e^{^{-\dfrac{2\epsilon^2}{\sum_{i=1}^n (2-0)^2}}} = e^{^{-\dfrac{\epsilon^2}{2n}}}. \]
%
Therefore:
\begin{align*}
P\left(\sum_{i=1}^n X_i+Y_i \geq n\right) & \qquad \\
& \nnngsp = P\left(\sum_{i=1}^n X_i+Y_i \geq \underbrace{(1-\Delta)n}_{n\mu}+\underbrace{n\Delta}_\epsilon\right) \\
& \nnngsp \leq e^{^{_{-\dfrac{\epsilon^2}{2n}}}} = e^{^{_{-\dfrac{n\Delta^2}{2}}}} < \left(1-\frac{\Delta^2}{4}\right)^n,
\end{align*}
where the last inequality is due to the fact that $\Delta^2/2 \in [0,\frac{1}{8}]$ and in the intervel $\left[0,1\right]$, we have $e^{-x} \leq 1-\frac{x}{2}$.

We now prove the left-most inequality in Expression \eqref{eqn:LUB}, by induction: the base case ($n = 1$) holds because $P(X_1+Y_1 \geq 2) = \frac{p}{2}$, since for $X_1+Y_1 \geq 2$ to hold, both $X_1$ and $Y_1$ must produce $1$. 
%Before getting getting to the induction step, let us introduce the random variables $S_n := X_1 + \cdots + X_n$ and $T_n := Y_1 + \cdots + Y_n$. 
For the inductive step, we show that for each $i$ and $j$ such that $i+j > n$, the following holds:
\begin{equation}
P\left(S_n = i+1, T_n = j \right) > \frac{p}{2} P\left(S_{n-1} = i, T_{n-1} = j \right)  \label{eqn:p/2}
\end{equation}
for each $k \in \{0,\ldots,n-2\}$, which proves the induction step since 

{\bf $k$ does not appear in Eq. 8!}
%
\[ P\left(\sum_{i=1}^{n-1} X_i+Y_i \geq n\right) = \sum_{i+j \geq n} P(S_{n-1}=i,T_{n-1}=j), \]
while we have
\begin{align*}
\sum_{i+j \geq n} \!\!\! P\left(S_n = i+1, T_n = j \right) & \leq \!\!\!\!\sum_{i+j \geq n+1} \!\!\!\! P\left(S_n = i, T_n = j \right) \\
 & \ngsp = P\left(\sum_{i=1}^{n} X_i+Y_i \geq n+1\right).
\end{align*}
\eqref{eqn:p/2} is then a consequence of the following chain of inequalities, in which $q := 1-p$:
\begin{align*}
& \frac{P\left(S_n = i+1, T_n = j \right)}{P\left(S_{n-1} = i, T_{n-1} = j \right)} = \frac{\binom{n}{i+1}p^{i+1} \cancel{q^{n-i-1}} \binom{n}{j} \frac{1}{2^n}}{\binom{n-1}{i} p^i \cancel{q^{n-1-i}} \binom{n-1}{j} \frac{1}{2^{n-1}}} \\
& \qquad\qquad = \frac{\binom{n}{i+1}p \binom{n}{j} \frac{1}{2}}{\binom{n-1}{i} \binom{n-1}{j}} = \frac{\frac{n!}{(i+1)!\cancel{(n-i-1)!}} \frac{n!}{\cancel{j!}(n-j)!}}{\frac{(n-1)!}{i!\cancel{(n-1-i)!}} \frac{(n-1)!}{\cancel{j!}(n-1-j)!}} \frac{p}{2} \\
& \qquad\qquad = \frac{\frac{n!}{(i+1)!} \frac{n!}{(n-j)!}}{\frac{(n-1)!}{i!} \frac{(n-1)!}{(n-1-j)!}} \frac{p}{2} = \frac{\frac{n!}{(n-1)!} \frac{n!}{(n-1)!}}{\frac{(i+1)!}{i!} \frac{(n-j)!}{(n-1-j)!}} \frac{p}{2} \\
& \qquad\qquad = \frac{n^2}{(i+1) (n-j)} \frac{p}{2} > \frac{p}{2}, \\
\end{align*}
where the last inequality follows from the fact that $i$ and $j$ range from $0$ to $n-1$.
\end{proof}
